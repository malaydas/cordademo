package net.corda.training.states

import net.corda.core.contracts.*
import net.corda.training.contracts.IOUContract
import net.corda.core.identity.Party
import net.corda.training.contracts.ProjectContract
import java.util.*

/**
 * This is where you'll add the definition of your state object. Look at the unit tests in [IOUStateTests] for
 * instructions on how to complete the [ProjectState] class.
 *
 * Remove the "val data: String = "data" property before starting the [ProjectState] tasks.
 */
@BelongsToContract(ProjectContract::class)
data class ProjectState(val allocatedAmount: Amount<Currency>,
                        val investor: Party,
                        val builder: Party,
                        val custody: Party,
                        val workCompletion: Int,
                        val paid: Amount<Currency> = Amount(0, allocatedAmount.token),
                        val totalPaid: Amount<Currency> = Amount(0, allocatedAmount.token),
                        override val linearId: UniqueIdentifier = UniqueIdentifier()): LinearState {
    /**
     *  This property holds a list of the nodes which can "use" this state in a valid transaction. In this case, the
     *  lender or the borrower.
     */
    override val participants: List<Party> get() = listOf(investor, builder, custody)

    /**
     * Helper methods for when building transactions for settling and transferring IOUs.
     * - [pay] adds an amount to the paid property. It does no validation.
     * - [withNewLender] creates a copy of the current state with a newly specified lender. For use when transferring.
     */
    fun pay(amountToPay: Amount<Currency>) = copy(paid = paid.plus(amountToPay))
    //fun withNewLender(newLender: Party) = copy(lender = newLender)
}